import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc';
import vitePluginRaw from 'vite-plugin-raw';
import * as path from "path";
import mdPlugin from 'vite-plugin-markdown';
// https://vitejs.dev/config/
export default defineConfig({
  base: process.env.NODE_ENV ? '' : '/marinails-net/',
  plugins: [
    react(),
    mdPlugin,
    vitePluginRaw({
      match: /\.md$/,
      exclude: [new RegExp(path.resolve(__dirname, './src/docs'))]
    })],
  build: {
    minify: false,
  },
})
