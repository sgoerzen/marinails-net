Preise
------

Hier finden Sie zuerst eine tabellarische Übersicht zu den Preisen.

### Preisübersicht

#### Handpflege / Maniküre

Express - Maniküre

10,00 €

Spa - Maniküre mit Handbad & Peeling

15,00 €

DeLuxe - Maniküre mit Paraffinbad

20,00 €

#### Nagelmodellage

Komplettmodellage / Nägelverlängerung

55,00 €

Auffüllung

37,00 €

Naturnagelverstärkung

32,00 €

Nail Art & Pinselmalerei

ab 40,00 €

Nagelreparatur pro Nagel

ab 3,00 €

Gel entfernen

15,00 €

#### Fußpflege / Pediküre

Pediküre

ab 20,00 €

Medizinische Fußpflege

ab 20,00 €

Fußpflege mit Hausbesuch möglich  
(Fahrkostenzuschlag 5,00 €)

Fuß-Modellage

ab 30,00 €
