**Anbieterkennzeichnung:**  
Hinweise gemäß §6 des Teledienstgesetzes (TDG)  
Nagelstudio Marinails



**Inhaberin:**  
Marina Moroz  
Marina Görzen  
Flachsstraße 2  
57537 Wissen  
Telefon 02742 -77 92 9 93  
E-Mail:  
Web: [http://www.marinails.net](http://www.marinails.net)



**Betreiber der Seite:**  
Marina Görzen  
Flachsstraße 2  
57537 Wissen

**Inhaltlich Verantwortliche gemäß §10 Absatz 3 MDStV:** Marina Görzen  
Marina Moroz  
(Anschrift wie oben)



**Haftungsausschluss:**  
Sofern auf Verweisziele ("Links") direkt oder indirekt verwiesen wird, die außerhalb des Veranwortungsbereiches des Betreibers liegen, haftet diese nur dann, wenn sie von den Inhalten Kenntnis hat und es ihr technisch möglich und zumutbar wäre, die Nutzung im Falle rechtwidriger Inhalte zu verhindern. Für darüber hinausgehende Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.



**Datenschutz:**  
Wir, die Betreiber von Marinails, nehmen den Schutz Ihrer persönlichen Daten sehr ernst und Halten uns strikt an die Regeln der Datenschutzgesetze. Personenbezogene Daten werden auf dieser Webseite nur im technisch notwendigen Umfang erhoben. In keinem Fall werden die erhobenen Daten verkauft jeder aus anderen Gründen an Dritte weitergegeben.



**Schutzrechte:**  
Alle Rechte an Markennamen und Markenbegriffen, der von uns auf diesen Seiten genannten Namen und Begriffe, liegen bei der jeweiligen Markeninhaberin.



**Copyright - Information:**  
Alle Seiten, inkl. Quellcode, Metadescription und Verfahren, unterliegen dem Copyright und sind nach dem Urheberrechtsgesetzt geschützt und dürfen ohne vorherige schriftliche Genehmigung von Marinails nicht weiterverwendet werden.


Webmaster and Developer: Sergej Görzen, [sergej.goerzen@gmail.com](mailto:sergej.goerzen@gmail.com)
