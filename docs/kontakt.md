Kontakt
-------

Hand- und Fußpflege,  
Marinails  
Flachsstraße 2  
57537 Wissen

Öffnungszeiten
--------------

Montag - Freitag: 10:00 bis 18:00 Uhr  
oder nach Vereinbarung Tel.: [02742 / 77 92 9 93](tel:+4927427792993)
