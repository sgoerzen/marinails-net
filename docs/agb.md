Allgemeine Geschäftsbedingungen
-------------------------------
**1\. Allgemeines**
Diese Bedingungen betreffen alle Transaktionen, Verträge und Angebote, wie die Dienstleistungen und den Verkauf von Produkten, die zwischen dem Nagelstudio und dem Kunden zustande kommen bzw. geschlossen werden. Ausnahmen von Transaktionen, die nicht unter die allgemeinen Geschäftsbedingungen fallen, bedürfen der Mitteilung in schriftlicher Form.

**2\. Dienstleistung**
Der Dienstleister (Nagelstudio oder Nail Designer) führt seine Dienstleistung nach bestem Wissen und Gewissen und nach dem neuesten Standard der Industrie am Kunden aus. Sollten Änderungen der Standards auftreten, wird der Dienstleister dem Kunden noch vor Beginn der Leistung davon in Kenntnis setzen.

**3\. Terminvereinbarung**
Termine werden vom Studio verbindlich vergeben. Sollte eine Absage bzw. ein Verschieben eines bestätigten Termins notwendig sein, sollte der Kunde dies rechtzeitig und sobald als möglich, mindestens 24 Stunden vor dem Termin, mitteilen. Sollte der Termin nicht rechtzeitig oder gar nicht abgesagt werden, hat der Dienstleister das Recht, den Verdienstausfall dem Kunden in Rechnung zu stellen.

Bei Verspätungen ab 15 Minuten muss unter Umständen ein neuer Termin vereinbart werden, sofern durch die Verspätung die rechtzeitige Wahrnehmung der weiteren, folgenden Kundentermine bzw. der betriebliche Ablauf nicht mehr gewährleistet werden kann. Das Nagelstudio hat in diesem Fall das Recht, den Preis für den entfallenen Termin in voller Höhe zu verlangen.

**4\. Preise**
Die Preise für Dienstleistungen und Produkte sind im Studio zu veröffentlichen. Die ausgewiesenen Preise müssen die gesetzliche Mehrwertsteuer enthalten. Preiserhöhungen müssen mindestens 30 Tage vor Inkrafttreten bekannt gegeben werden. Angebote sind nur in dem veröffentlichen Zeitraum gültig und gelten solange der Vorrat reicht. Der Kunde muss direkt am Anschluss der Behandlung, bzw. direkt beim Kauf für den Service, bzw. das Produkt bezahlen. Eine Teilzahlungsvereinbarung ist in Einzelfällen nach Absprache möglich.

**5\. Persönliche Daten und Privatsphäre**
Der Kunde versichert alle persönlichen Daten und Informationen, die relevant für die professionelle Behandlung der geforderten Dienstleistung sind, an das Studio und/oder den Dienstleister weiterzugeben. Diese Daten werden auf der Kundenkarteikarte in schriftlicher oder elektronischer Form gespeichert. Das Studio bzw. der Dienstleister verpflichtet sich, diese Daten nur für den Zweck der zu erbringenden Dienstleistung unter Berücksichtigung der Datenschutzklauseln einzusetzen und die Daten nicht an Dritte oder außen stehende Personen weiterzugeben ohne schriftliche Einwilligung des Kunden.

**6\. Vertraulichkeit**
Das Nagelstudio, seine Angestellten und die Geschäftsleitung sind verpflichtet, alle Informationen, die während der Behandlung oder dem Service besprochen wurden, geheim zu halten und vertraulich zu behandeln. Informationen werden als vertraulich eingestuft, wenn der Kunde dies zum Ausdruck bringt, bzw. wenn dies aus dem Inhalt der Informationen zu folgern ist. Die Vertraulichkeit wird außer Kraft gesetzt, wenn dies von Rechtswegen entschieden wird und das Nagelstudio von der Vertraulichkeitsklausel entbunden wird.

**7\. Haftung**
Das Nagelstudio, seine Angestellten und die Geschäftsleitung können keine Haftung übernehmen, wenn der Kunde durch eine Dienstleistung zu Schaden kommt, die auf vom Kunden gelieferten Informationen beruht und sich diese als unzureichend herausstellen. Dies bezieht sich vor allen Dingen, aber nicht ausschließlich, auf physische Bedingungen, medizinische oder medikamentöse Voraussetzungen oder Aktivitäten außerhalb des Studios. Das Nagelstudio ist nicht verantwortlich für den Verlust oder die Zerstörung persönlichen Gegenständen und von Besitz des Kunden die (der) mit in das Studio gebracht wurden (wurde).

**8\. Garantie**
Auf den vom Nagelstudio geleisteten Service und auf die verkauften Produkte erhält der Kunde eine Garantie von 7 Tagen. Diese Garantie verfällt wenn:
1) Der Kunde die Modellage von einem anderen Nagelstudio oder Nail Designer behandeln oder auffüllen lässt.
2) Der Kunde ohne Gebrauch von Handschuhen mit Chemikalien in Berührung kommt oder damit arbeitet.
3) Der Kunde die Modellage unsachgemäß entfernt.
4) Der Kunde andere als die vom Nagelstudio oder Nail Designer empfohlenen Produkte verwendet, um die Modellage zu pflegen.
5) Der Kunde den Hinweis in Notfällen unverzüglich medizinische Hilfe in Anspruch zu nehmen nicht nachkommt.
6) Der Kunde die Produkte nicht unter Einhaltung der Gebrauchshinweise anwendet.

**9\. Beschädigung und Diebstahl**
Das Nagelstudio hat das Recht für alle vom Kunden verursachten Schäden eine Wiedergutmachung zu fordern. Ladendiebstähle werden zur Anzeige gebracht.

**10\. Beschwerden und Reklamationen**
Sollte der Kunde eine Beschwerde oder eine Reklamation über den Service, bzw. das gekauftem Produkt haben, so muss dies schnellstmöglich, spätestens aber 5 Tage nach Erkennen des Reklamationsgrundes der Geschäftsleitung, bzw. dem Nail Designer gemeldet werden. Das Studio muss innerhalb der darauf folgenden 5 Arbeitstage eine akzeptable Lösung unterbreiten, um die Reklamation auszuräumen. Ist eine Reklamation berechtigt, muss das Nagelstudio die reklamierte Arbeit ohne zusätzliche Zahlung erneut leisten, bzw. das reklamierte Produkt ersetzen. Sollte die Reklamation nicht zur Zufriedenheit des Kunden bereinigt werden, kann dies auch von einer Schiedsstelle oder Gericht entschieden werden.

**11\. Nail Art**
Wenn ein Nagelstudio oder der Nail Designer Nail-Art Beispiele im Studio veröffentlicht, ist dies nur ein Beispiel. Selbst wenn das Design am Kunden nachgearbeitet wird, kann es zu Abweichungen bezüglich des Designs, der Form und der Wirkung des Designs kommen. Das Resultat kann von der gezeigten Vorlage abweichen. Das Urheberrecht für das Design verbleibt beim Studio, bzw. beim Nail Designer. Sollte der Kunde vom erstellten Design Fotographien anfertigen lassen, die für kommerzielle Zwecke verwendet werden sollten, bedarf dies der schriftlichen Einverständniserklärung durch das Studio, bzw. den Nail Designer. Bei Unterlassen kann das Studio angemessene Ansprüche geltend machen.

**12\. Verhaltensweisen**
Der Kunde verpflichtet sich während seines Besuchs im Studio eine angemessene Verhaltensweise an den Tag zu legen. Sollte der Kunde selbst nach einer Verwarnung weiterhin nicht angemessen verhalten, hat die Geschäftsleitung das Recht, den Kunden aus dem Studio zu verweisen und gegebenenfalls ein Hausverbot auszusprechen.

**13\. Gerichtsstand**
Für die gerichtliche Klärung von Unstimmigkeiten ist der Gerichtsstand der Ort des Nagelstudios. Geltendes Recht ist Ortsansässigkeit zu bestimmen.
