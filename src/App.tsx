import 'bootstrap/dist/css/bootstrap.css';
import "react-image-gallery/styles/scss/image-gallery.scss";

import './App.scss'

import moment from "moment";
import "moment/locale/de";
moment.locale("de");

import Navbar from "./components/Navigation";
import Header from "./components/Header";
import StickyButton from "./components/StickyButton";
import Popup from "./components/Popup";

// include partials
import Gallerie from "./partials/Gallerie";
import Home from "./partials/Home";
import Kontakt from "./partials/Kontakt";
import News from "./partials/News";
import Preise from "./partials/Preise";
import Gmaps from "./partials/GMaps.js";

import galleryItems from "./assets/js/gallery-items.json";

import impressumMd from "../docs/impressum.md";
import agbMd from "../docs/agb.md";
import datenschutzMd from "../docs/datenschutz.md";
import {INewsItem} from "./components/NewsSlider";

export function getServerSideProps() {
    // fetch news
    const newsItems: INewsItem[] = [{message: <>
            Besuchen Sie unseren Telegram Kanal auf <a href='https://t.me/nagelstudio_marinails'>@nagelstudio_marinails</a>.
        </>, created_time: Date.now(), id: "442342s"}];

    return {
        props: {
            newsItems,
            galleryItems,
            popups: {
                impressum: impressumMd.html,
                agb: agbMd.html,
                datenschutz: datenschutzMd.html,
            }
        }
    };
}
export default function App() {
    const rtn = getServerSideProps();
    const {popups, newsItems} = rtn.props;
    return  <>
        <Header />
        <Navbar />

        <main>
            <h1 id="title" className="title">Marinails</h1>
            <Home />
            <News items={newsItems} />
            <Preise />
            <Gallerie items={galleryItems} />
            <Kontakt />
            <Gmaps />
        </main>

        <footer className="footer">
            <div className="container">
                &copy; Sergej Görzen
                &nbsp;|&nbsp;
                <Popup linkText="Impressum" title="Impressum">
                    <div dangerouslySetInnerHTML={{ __html: popups.impressum }}></div>
                </Popup>
                &nbsp;|&nbsp;
                <Popup linkText="Datenschutz" title="Datenschutz">
                    <div dangerouslySetInnerHTML={{ __html: popups.datenschutz }}></div>
                </Popup>
                &nbsp;|&nbsp;
                <Popup linkText="AGB" title="AGB">
                    <div dangerouslySetInnerHTML={{ __html: popups.agb }}></div>
                </Popup>
            </div>
        </footer>

        <StickyButton />
    </>
}
