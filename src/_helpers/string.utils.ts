export function toBoolean(str: string | boolean | number | undefined) {
    if (!str)
        return false;
    return str === true || str === 1 || (typeof(str) === "string" && str.toLowerCase() === "true") || str === 1 || str === "1";
}
