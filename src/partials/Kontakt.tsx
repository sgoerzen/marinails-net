import {Container, Row, Col} from "reactstrap";

export default function Kontakt() {
    const boxInnerStyle = {
        minHeight: "113px"
    };
    return <section id="kontakt">
        <Container>
            <Row>
                <Col sm={6}>
                    <div className="box white-box text-center">
                        <h2>Kontakt</h2>
                        <div style={boxInnerStyle}>
                            <p>Hand- und Fußpflege,<br />
                                Marinails<br />
                                Flachsstraße 2<br />
                                57537 Wissen<br />
                                Tel.: <a href="tel:+4927427792993">02742 / 77 92 9 93</a>
                            </p>
                        </div>
                        <hr />
                    </div>
                </Col>
                <Col sm={6}>
                    <div className="box white-box text-center">
                        <h2>Öffnungszeiten</h2>
                        <div style={boxInnerStyle}>
                            <p>Dienstag - Freitag: 10:00 bis 18:00 Uhr<br />
                                Termine nach Vereinbarung<br />
                                Tel.: <a href="tel:+4927427792993">02742 / 77 92 9 93</a>
                            </p>
                        </div>
                        <hr />
                    </div>
                </Col>
            </Row>
        </Container>
    </section>
}
