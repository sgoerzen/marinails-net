import {Container, Row, Col} from "reactstrap";

export default function Preise() {
    const boxStyle = {
        minHeight: "244px"
    };
    return <section id="preise">
        <Container>
            <Row>
                <Col>
                    <h2 style={{display: "none"}}>Preise</h2>
                </Col>
            </Row>
            <Row>
                <Col sm={4}>
                    <div className="box lila-box price-table" style={boxStyle}>
                        <h3>Maniküre</h3>
                        <div className="price-table-content">
                            <div><span className="description">Spa - Maniküre</span><span className="price">ab 25,00 &euro;*</span></div>
                            <div><span className="description">DeLuxe - Maniküre</span><span className="price">ab 30,00 &euro;*</span></div>
                            <div><span className="description">Maniküre mit Shellac</span><span className="price">ab 30,00 &euro;*</span></div>
                        </div>
                        <hr/>
                        <small>* Der Preis richtet sich nach Länge, Form und Aufwand.</small>
                    </div>
                </Col>
                <Col sm={4}>
                    <div className="box light-lila-box price-table" style={boxStyle}>
                        <h3>Pediküre</h3>
                        <div className="price-table-content">
                            <div><span className="description">Pediküre</span><span className="price">ab 35,00 &euro;*</span></div>
                            <div><span className="description">Fuß-Modellage</span><span className="price">ab 35,00 &euro;*</span></div>
                        </div>
                        <hr/>
                        <small>* Der Preis richtet sich nach Länge, Form und Aufwand.</small>
                    </div>
                </Col>
                <Col sm={4}>
                    <div className="box white-box price-table" style={boxStyle}>
                        <h3>Nagelmodellage</h3>
                        <div className="price-table-content">
                            <div><span className="description">Komplettmodellage/Nagelverlängerung</span><span className="price">ab 60,00 &euro;*</span></div>
                            <div><span className="description">Auffüllung</span><span className="price">ab 40,00 &euro;*</span></div>
                            <div><span className="description">Naturnagelverstärkung</span><span className="price">ab 35,00 &euro;*</span></div>
                            <div><span className="description">Nail Art & Pinselmalerei</span><span className="price">ab 45,00 &euro;*</span></div>
                            <div><span className="description">Nagelreparatur pro Nagel</span><span className="price">ab 5,00 &euro;*</span></div>
                            <div><span className="description">Gel entfernen</span><span className="price">20,00 &euro;</span></div>
                        </div>
                        <hr/>
                        <small>* Der Preis richtet sich nach Länge, Form und Aufwand.</small>
                    </div>
                </Col>
            </Row>
        </Container>
    </section>
}
