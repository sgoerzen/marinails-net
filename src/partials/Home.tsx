import {Container, Row, Col} from "reactstrap";

export default function Home() {
    return <section id="home">
        <Container>
            <Row>
                <Col>
                    <div className="box light-lila-box">
                        <h2>Herzlich Willkommen im Nagelstudio in Wissen!</h2>
                        <div>
                            <p>Herzlich willkommen in unserem exklusiven Studio, wo wir Ihnen nicht nur professionelle Beratung, sondern auch handwerkliche Meisterleistungen in den Bereichen Nagelmodellage und Fußpflege bieten.</p>
                            <p>Seit unserer Gründung im Jahr 2010 sind wir stolz darauf, ein fester Bestandteil der Gemeinschaft in Wissen zu sein. Ein herzliches Dankeschön gilt all unseren geschätzten Kunden, die uns durch ihre Treue unterstützen und zu unserem Erfolg beitragen.</p>
                            <p>In unserem Studio erwartet Sie nicht nur eine angenehme Atmosphäre, sondern auch ein erfahrenes Team, das sich mit Hingabe um Ihre individuellen Bedürfnisse kümmert. Wir legen großen Wert darauf, Ihnen nicht nur ein ästhetisches, sondern auch ein entspannendes Erlebnis zu bieten.</p>
                            <p>Besuchen Sie uns gerne persönlich, um die Vielfalt unserer Leistungen zu entdecken und sich von unserem Engagement für höchste Qualität zu überzeugen. Folgen Sie uns auch auf unserer Facebook-Seite, um stets über aktuelle Angebote, Tipps und Neuigkeiten informiert zu sein.</p>
                            <p>Wir freuen uns darauf, Sie in unserem Studio begrüßen zu dürfen und Ihnen eine erstklassige Beauty-Erfahrung zu bieten.</p>
                        </div>
                        <hr />
                    </div>
                </Col>
            </Row>
        </Container>
    </section>
}
