import {Container, Row, Col} from "reactstrap";
import NewsSlider, {INewsItem} from "../components/NewsSlider";

export default function News({ items }: { items: INewsItem[] }) {
    const boxStyle = {
        minHeight: "284px"
    };
    return <section id="news">
        <Container>
            <Row>
                <Col sm={8}>
                    <div className="box white-box" style={boxStyle}>
                        <h2>News</h2>
                        <NewsSlider items={items} />
                    </div>
                </Col>
                <Col sm={4}>
                    <div className="box lila-box" style={boxStyle}>
                        <h2>Corona</h2>
                        <div style={{minHeight: "183px"}}>
                            <p><strong>Stand: 03. April 2022</strong></p>
                            <p>Derzeit gibt es keine Pflichtbeschränkungen, jedoch empfehlen wir das Tragen von Masken.</p>
                            <p><a target="_blank" href="https://corona.rlp.de/de/aktuelles/corona-regeln-im-ueberblick/">Hier</a> finden Sie mehr Details.</p>
                        </div>
                        <hr />
                    </div>
                </Col>
            </Row>
        </Container>
    </section>
}
