
export default function GMaps() {
    return <section className="gmaps" style={{
        background: 'url(../images/gmaps.png) no-repeat 50%',
        backgroundSize: 'cover',
        height: '300px',
        marginBottom: 0,
        borderTop: "1px solid #A61C68"
    }} />;
}
