import Gallery from "../components/Gallery";

export default function Gallerie({items}: { items: string[]}) {
    return <section id="gallerie">
        <hr />
        <Gallery items={items} />
        <hr />
    </section>
}
