import {A11y, Navigation, Pagination} from "swiper/modules";
import {Swiper, SwiperSlide} from "swiper/react";

import moment from "moment";
import {ReactNode} from "react";

export interface INewsItem {
    id: string;
    created_time: number;
    message: ReactNode;
}

export default function NewsSlider(props: { items: INewsItem[] } = {items: []}) {
    return (<Swiper
        modules={[Navigation, Pagination, A11y]}
        spaceBetween={50}
        slidesPerView={1}
        navigation
        loop={false}
        pagination={{ clickable: false }}
        scrollbar={{ draggable: true }}
        onSwiper={(swiper) => console.log(swiper)}
        onSlideChange={() => console.log('slide change')}
        style={{minHeight: "210px"}}
    >
        {
            props.items.map(item => {
                const now = !import.meta.env.DEV ? moment(item.created_time).fromNow() : "now";
                return (
                    <SwiperSlide key={item.id} style={{padding: "0 3.5em"}}>
                        <div className="text">
                            <p><strong>{now}</strong></p>
                            <p style={{textAlign: "justify"}}>{item.message}</p>
                        </div>
                    </SwiperSlide>
                );
            })
        }
    </Swiper>);
}
