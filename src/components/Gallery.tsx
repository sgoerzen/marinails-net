import ImageGallery from "react-image-gallery";

export interface IRemoteImage {
    source: string;
    width: number;
    height: number;
}
export interface IRemoteItem {
    image: IRemoteImage;
    thumbnail: IRemoteImage;
}
export interface IGalleryItem {
    original: string;
    thumbnail: string;
    originalHeight?: number;
    originalWidth?: number;
};

export default function Gallery({items}: { items: string[] }) {
    const images: IGalleryItem[] = items.map((url) => {
        return {
            original: url,
            thumbnail: url,
        };
    });
    return <div id="gallery">
        <ImageGallery
            items={images}
            infinite={true}
            lazyLoad={true}
            autoPlay={true}
            showBullets={false}
        />
    </div>;
}
