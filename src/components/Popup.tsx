import {Modal} from "react-bootstrap";

import {useState} from "react";

import "../styles/Popup.scss";

export interface IPopupParams {
    linkText: string;
    title: string;
    children: any;
}
export default function Popup({linkText, title, children}: IPopupParams) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <a onClick={handleShow}>
                {linkText}
            </a>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{children}</Modal.Body>
            </Modal>
        </>
    );
}
