import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, A11y, Parallax } from 'swiper/modules';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/parallax';

import "../styles/SwiperBanner.scss";

//        <div id="banner" className="parallax-bg" data-swiper-parallax="-23%"></div>
export default function SwiperBanner() {
    return <Swiper
        modules={[Navigation, Pagination, A11y, Parallax]}
        spaceBetween={50}
        slidesPerView={1}
        navigation
        parallax
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={(swiper) => console.log(swiper)}
        onSlideChange={() => console.log('slide change')}
        style={{backgroundImage: 'url(/images/banner.jpg)', minHeight: "300px"}}
    >
        <SwiperSlide>
            <div className="title" data-swiper-parallax="-300">Gepflegte Hände</div>
            <div className="subtitle" data-swiper-parallax="-200">sind das A und O</div>
            <div className="text" data-swiper-parallax="-100">
                <p>Gepflegte Hände und formschöne Fingernägel vervollständigen das Gesamtbild eines jeden Menschen.
                    Brüchige, rissige, glanzlose und verformte Fingernägel werfen dabei kein gutes Licht ab. Oft
                    passiert dies durch falsche oder mangelnde Pflege.</p>
            </div>
        </SwiperSlide>
        <SwiperSlide>
            <div className="title" data-swiper-parallax="-300" data-swiper-parallax-opacity="0">Nailart</div>
            <div className="subtitle" data-swiper-parallax="-200">Die Verzierung des fertigen Nagels nennt man
                Nailart.
            </div>
            <div className="text" data-swiper-parallax="-100">
                <p></p>
            </div>
        </SwiperSlide>
        <SwiperSlide>
            <div className="title" data-swiper-parallax="-300">Schlicht und Schick</div>
            <div className="subtitle" data-swiper-parallax="-200">oder auch Extravagant</div>
            <div className="text" data-swiper-parallax="-100">
                <p>Die schlichteste Variante ist der Frenchlook, bei dem die Nagelspitze aus natur- bis strahlendweißem,
                    der Rest aus transparentem oder leicht milchigem Material verarbeitet wird.
                    Eine Nageldesignerin kann einen Kunstnagel auch so aussehen lassen, als wäre er ein völlig
                    natürlicher Nagel.
                    Jedoch gibt es für die Kreativität der Designerin und dem Kunden kaum Grenzen, die Nägel können
                    beklebt, mit Farben und Glitter verziert und in viele extravagante Formen gebracht werden.
                </p>
            </div>
        </SwiperSlide>
    </Swiper>
}
