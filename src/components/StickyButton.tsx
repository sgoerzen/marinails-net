import "../styles/StickyButton.scss";

export default function StickyButton() {
    return (
        <a href="#" className="phoneButton">
            <div className="sticky-button">
                <span className="fas fa-phone"/>
            </div>
        </a>
    );
}
