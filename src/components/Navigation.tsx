import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {useEffect, useState} from "react";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebook, faInstagram, faTiktok, faTelegram} from "@fortawesome/free-brands-svg-icons";

import {scroller} from "react-scroll";

import "../styles/Navigation.scss";

export interface INavLink {
    content: string;
    href: string;
    target?: string;
    className?: string;
}

export default function Navigation() {

    const links = [
        {content: "Home", href: "#home"},
        {content: "News", href: "#news"},
        //{ content: "Angebote", href: "#angebote"},
        {content: "Preise", href: "#preise"},
        {content: "Galerie", href: "#galerie"},
        {content: "Kontakt", href: "#kontakt"}
    ];

    const socialLinks = [
        {
            content: <FontAwesomeIcon icon={faFacebook}/>,
            href: "https://www.facebook.com/nailartmarinails",
            target: '_blank',
            className: "brand-link"
        },
        {
            content: <FontAwesomeIcon icon={faInstagram}/>,
            href: "https://www.instagram.com/nailart_marinails/",
            target: '_blank',
            className: "brand-link"
        },
        {
            content: <FontAwesomeIcon icon={faTiktok}/>,
            href: "https://tiktok.com/@nailart_marinails ",
            target: '_blank',
            className: "brand-link"
        },
        {
            content: <FontAwesomeIcon icon={faTelegram}/>,
            href: "https://t.me/nagelstudio_marinails",
            target: '_blank',
            className: "brand-link"
        },
    ];

    const [selectedLinkIndex, selectLinkIndex] = useState(0);

    const updateSelectedIndex = (e: any, i: any) => {
        if (i < 0 || i >= links.length)
            return;

        const element = links[i];
        const selectable = element.href.indexOf('#') > -1;
        const id = element.href.substring(1);

        if (!selectable)
            return;

        e?.preventDefault();

        selectLinkIndex(i);

        if (element.href === '#home') {
            scroller.scrollTo("__next", {
                smooth: true,
                offset: 0,
                duration: 500,
            });
        } else {
            scroller.scrollTo(id, {
                smooth: true,
                offset: -110,
                duration: 500,
            });
        }

    };

    let [isOpen, setIsOpen] = useState(false);
    const [sticky, setSticky] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        const hashIndex = links.findIndex(l => l.href === window.location.hash);
        updateSelectedIndex(null, hashIndex);
    });

    const handleScroll = () => {
        const height = document.getElementsByTagName('header')[0].clientHeight;
        setSticky(window.scrollY > height);
    }

    return (
        <div className={`header${sticky ? ' sticky' : ''}`}
             style={{borderTop: '2px solid white', borderBottom: '2px solid white'}}>
            <Navbar light expand="md">
                <Container>
                    <NavbarBrand href="/" style={{float: 'left'}}>
                        <img className="brand" src="/images/BigLogo.png" alt="Brand"/>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle}/>
                    <Collapse isOpen={isOpen} navbar style={{float: 'right'}}>
                        <Nav className="m-auto" navbar>
                            {
                                links.map(({content, href}, index) => {
                                    return <NavItem key={index}>
                                        <NavLink className={(selectedLinkIndex === index ? 'active' : '')}
                                                 href={href}
                                                 onClick={e => updateSelectedIndex(e, index)}
                                        >{content}</NavLink>
                                    </NavItem>
                                })
                            }
                            <div className="clear"></div>
                            {
                                socialLinks.map(({content, href, target}, index) => {
                                    return <NavItem key={index}>
                                        <NavLink className="social-link brand-link" target={target} href={href}>{content}</NavLink>
                                    </NavItem>
                                })
                            }
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}
