$(document).ready(function () {
    $('.sticky').sticky({
        topSpacing: 0
    });

    new Swiper('#bannerWrapper', {
        speed: 600,
        autoplay: {
            delay: 3000
        },
        parallax: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $('nav li.nav-item a.nav-link').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.attr("href");
        $('html,body').animate({
            scrollTop: $(id).offset().top - 64
        });
        return false;
    });

});

