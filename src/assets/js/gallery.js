$(document).ready(function () {


    $.getJSON("./images.json" ,function (data) {
        data.gallery.forEach((image) => {
            appendImageToGallery($('#gallery .gallery-top .swiper-wrapper'), image.url);
            appendImageToGallery($('#gallery .gallery-thumbs .swiper-wrapper'), image.url);
        });
        initGallery();
    });
});

function appendImageToGallery($container, url) {
    var html = `<div class="swiper-slide" style="background-image:url(${url})"></div>`;
    $container.append(html);
}
function initGallery() {
    //  https://idangero.us/swiper/demos/
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 12,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        loop: true,
        grabCursor: true,
        preloadImages: false
    });
    new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        },
        autoplay: {
            delay:5000
        }
    });
}
