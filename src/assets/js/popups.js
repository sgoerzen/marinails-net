$(document).ready(function () {

    $('.popup').each(function () {
        var $btn = $('<button class="closeBtn"></button>');
        var $this = $(this);
        $this.prepend($btn);

        $btn.click(function () {
            $this.removeClass('open');
        });
    });

    $('[data-popup]').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var target = $this.data('popup');
        $(target).addClass('open');
        return false;
    });
});

